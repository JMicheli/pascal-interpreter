﻿using System;
using System.Collections.Generic;

using Interpreter.Internal;

namespace Interpreter.AST
{
    public enum ASTNodeType
    {
        None,

        Program,
        Block,
        VarDecl,
        TypeNode,
        Param,
        ProcedureDecl,
        ProcedureCall,
        Compound,
        Assign,
        Var,
        BinOp,
        UnOp,
        Num,

        NoOp
    }

    public abstract class Node
    {
        public ASTNodeType Type = ASTNodeType.None;
    }

    class Program : Node
    {
        public string Name;
        public Block Block;

        public Program(string name, Block block)
        {
            Name = name;
            Block = block;
            Type = ASTNodeType.Program;
        }
    }

    class Block : Node
    {
        public List<Node> DeclarationNodes;
        public Compound CompoundStatement;

        public Block(List<Node> declaration_nodes, Compound compound_statement)
        {
            DeclarationNodes = declaration_nodes;
            CompoundStatement = compound_statement;
            Type = ASTNodeType.Block;
        }
    }

    class VarDecl : Node
    {
        public Var VarNode;
        public TypeNode TypeNode;

        public VarDecl(Var var, TypeNode type)
        {
            VarNode = var;
            TypeNode = type;
            Type = ASTNodeType.VarDecl;
        }
    }

    class TypeNode : Node
    {
        public Token Token;
        public string Value;

        public TypeNode(Token token)
        {
            Token = token;
            Value = token.Value;
            Type = ASTNodeType.TypeNode;
        }
    }

    class Param : Node
    {
        public Var VarNode;
        public TypeNode TypeNode;

        public Param(Var var, TypeNode type)
        {
            VarNode = var;
            TypeNode = type;
            Type = ASTNodeType.Param;
        }
    }

    class ProcedureDecl : Node
    {
        public string ProcedureName;
        public List<Param> Parameters;
        public Block BlockNode;

        public ProcedureDecl(string name, List<Param> parameters, Block block)
        {
            ProcedureName = name;
            Parameters = parameters;
            BlockNode = block;
            Type = ASTNodeType.ProcedureDecl;
        }
    }

    class ProcedureCall : Node
    {
        public string ProcedureName;
        public List<Node> ActualParameters;
        public Token Token;
        public ProcedureSymbol ProcedureSymbol;

        public ProcedureCall(string name, List<Node> parameters, Token token)
        {
            ProcedureName = name;
            ActualParameters = parameters;
            Token = token;
            Type = ASTNodeType.ProcedureCall;
        }
    }

    class Compound : Node
    {
        public List<Node> Nodes;

        public Compound(List<Node> nodes)
        {
            Nodes = nodes;
            Type = ASTNodeType.Compound;
        }
    }

    class Assign : Node
    {
        public Var Left;
        public Token Op;
        public Node Right;

        public Assign(Var left, Token op, Node right)
        {
            Left = left;
            Op = op;
            Right = right;
            Type = ASTNodeType.Assign;
        }
    }

    class Var : Node
    {
        public Token Token;
        public string Name;

        public Var(Token token)
        {
            Token = token;
            Name = (string)token.Value;
            Type = ASTNodeType.Var;
        }
    }

    class BinOp : Node
    {
        public Node Left;
        public Token Op;
        public Node Right;

        public BinOp(Node left, Token op, Node right)
        {
            Left = left;
            Op = op;
            Right = right;
            Type = ASTNodeType.BinOp;
        }
    }

    class UnOp : Node
    {
        public Token Op;
        public Node Right;

        public UnOp(Token op, Node right)
        {
            Op = op;
            Right = right;
            Type = ASTNodeType.UnOp;
        }
    }

    class Num : Node
    {
        public Token Token;
        public float Value;

        public Num(Token token)
        {
            Token = token;
            Value = (float)token.Value;
            Type = ASTNodeType.Num;
        }
    }

    class NoOp : Node
    {
        public NoOp()
        {
            Type = ASTNodeType.NoOp;
        }
    }
}
