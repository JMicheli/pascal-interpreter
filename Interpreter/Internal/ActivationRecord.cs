﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Internal
{
    enum ARType
    {
        PROGRAM,
        PROCEDURE
    }

    class ActivationRecord
    {
        #region Private Members

        private Dictionary<string, dynamic> mMembers;

        #endregion

        #region Public Overrides

        public dynamic this[string key]
        {
            get
            {
                if (mMembers.ContainsKey(key))
                    return mMembers[key];
                else
                    return null;
            }

            set
            {
                if (mMembers.ContainsKey(key))
                    mMembers[key] = value;
                else
                    mMembers.Add(key, value);
            }
        }

        public override string ToString()
        {
            return string.Format("{0}: {1} {2}", NestingLevel, Type.ToString("F"), Name);
        }

        #endregion

        #region Public Properties

        public string Name;
        public ARType Type;
        public int NestingLevel;

        #endregion

        #region Constructor

        public ActivationRecord(string name, ARType type, int nesting_level)
        {
            Name = name;
            Type = type;
            NestingLevel = nesting_level;

            mMembers = new Dictionary<string, dynamic>();
        }

        #endregion Constructor
    }
}
