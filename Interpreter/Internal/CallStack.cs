﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Internal
{
    class CallStack
    {

        #region Private Members

        private List<ActivationRecord> mRecords;

        #endregion

        #region Public Overrides

        public override string ToString()
        {
            string output = "CALL STACK\n";
            foreach (ActivationRecord ar in mRecords)
                output += ar.ToString() + "\n";

            return output;
        }

        #endregion

        #region Public Fns

        public void Push(ActivationRecord ar)
        {
            mRecords.Add(ar);
        }

        public ActivationRecord Pop()
        {
            int index = mRecords.Count - 1;
            ActivationRecord ar = mRecords[index];
            mRecords.RemoveAt(index);

            return ar;
        }

        public ActivationRecord Peek()
        {
            int index = mRecords.Count - 1;
            return mRecords[index];
        }

        #endregion

        #region Constructor

        public CallStack()
        {
            mRecords = new List<ActivationRecord>();
        }

        #endregion

    }
}
