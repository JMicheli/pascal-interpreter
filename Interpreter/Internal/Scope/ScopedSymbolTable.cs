﻿using System.Collections.Generic;

namespace Interpreter.Internal
{

    class ScopedSymbolTable
    {
        private Dictionary<string, Symbol> mSymbols;

        public string ScopeName;
        public int ScopeLevel;
        public ScopedSymbolTable EnclosingScope;

        public ScopedSymbolTable(string name, int level, ScopedSymbolTable enclosing_scope = null)
        {
            //Initialize table
            mSymbols = new Dictionary<string, Symbol>();
            ScopeName = name;
            ScopeLevel = level;
            EnclosingScope = enclosing_scope;

            //Set built-ins
            Insert(new BuitInTypeSymbol("INTEGER"));
            Insert(new BuitInTypeSymbol("REAL"));
        }

        public void Insert(Symbol symbol)
        {
            symbol.ScopeLevel = ScopeLevel;
            if (mSymbols.ContainsKey(symbol.Name))
                mSymbols[symbol.Name] = symbol;
            else
                mSymbols.Add(symbol.Name, symbol);
        }

        public Symbol Lookup(string name, bool current_scope_only=false)
        {
            if ((name != null) && (mSymbols.ContainsKey(name))) //Check self
                return mSymbols[name];
            else if (current_scope_only)
                return null;
            else if ((name != null) && (EnclosingScope != null)) //Check next scope up
                return EnclosingScope.Lookup(name);
            else //If it's a null lookup or the variable is undefined
                return null;
        }

        public override string ToString()
        {
            string title = string.Format("(Scoped Symbol Table) {0} level {1}\n", ScopeName, ScopeLevel);

            string subtitle = "";
            if (EnclosingScope != null)
                subtitle = string.Format("Enclosing scope: {0}\n", EnclosingScope.ScopeName);

            string lines = new string('=', title.Length) + "\n";
            string output = lines + title + subtitle + lines;

            if (mSymbols.Count == 0)
            {
                output += "SYMBOL TABLE EMPTY\n" + lines;
                return output;
            }
            
            foreach (Symbol symbol in mSymbols.Values)
                output += symbol.ToString() + "\n";
            output += lines;

            return output;
        }
    }
}
