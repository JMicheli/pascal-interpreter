﻿using System;
using System.Collections.Generic;

namespace Interpreter.Internal
{
    abstract class Symbol
    {
        public string Name;
        public Symbol Type;
        public int ScopeLevel;

        public Symbol(string name, Symbol type =null)
        {
            Name = name;
            Type = type;
        }

        public override string ToString()
        {
            return String.Format("<{0} : {1}>", Name, Type);
        }
    }

    class ProcedureSymbol : Symbol
    {
        public List<Symbol> FormalParameters;
        public AST.Block BlockNode;

        public ProcedureSymbol(string name, List<Symbol> parameters)
            : base(name)
        {
            FormalParameters = parameters;
        }

        public ProcedureSymbol(string name)
            : base(name)
        {
            FormalParameters = new List<Symbol>();
            BlockNode = null;
        }

        public override string ToString()
        {
            string output = string.Format("ProcedureSymbol name={0}, parameters=", Name);
            foreach (Symbol s in FormalParameters)
                output += s.ToString() + " ";
            return output;
        }
    }

    class VarSymbol : Symbol
    {
        public VarSymbol(string name, Symbol type)
            : base(name, type)
        {
        }

        public override string ToString()
        {
            return String.Format("<{0} : {1}>", Name, Type.Name);
        }
    }

    class BuitInTypeSymbol : Symbol
    {
        public BuitInTypeSymbol(string name)
            : base(name)
        {
        }

        public override string ToString()
        {
            return String.Format("<BUILT IN TYPE: {0}>", Name);
        }
    }
}
