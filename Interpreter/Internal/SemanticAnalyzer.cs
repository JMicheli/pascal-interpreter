﻿using System;
using Interpreter.AST;

namespace Interpreter.Internal
{

    public static class SemanticAnalyzer
    {
        private static ScopedSymbolTable mCurrentScope;

        public static void ClearGlobal()
        {
            mCurrentScope = null;
        }

        static SemanticAnalyzer()
        {
            mCurrentScope = null;
        }

        public static void Visit(Node node)
        {
            switch (node.Type)
            {
                case ASTNodeType.None:
                    throw new Exception("Parser error: SymbolTableBuilder tried to visit a generic node");
                case ASTNodeType.Program:
                    Visit_Program((Program)node);
                    return;
                case ASTNodeType.Block:
                    Visit_Block((Block)node);
                    return;
                case ASTNodeType.VarDecl:
                    Visit_VarDecl((VarDecl)node);
                    return;
                case ASTNodeType.ProcedureDecl:
                    Visit_ProcedureDecl((ProcedureDecl)node);
                    return;
                case ASTNodeType.ProcedureCall:
                    Visit_ProcedureCall((ProcedureCall)node);
                    return;
                case ASTNodeType.TypeNode:
                    throw new Exception("SymbolTableBuilder attempted to visit TypeNode");
                case ASTNodeType.Compound:
                    Visit_Compound((Compound)node);
                    return;
                case ASTNodeType.Assign:
                    Visit_Assign((Assign)node);
                    return;
                case ASTNodeType.Var:
                    Visit_Var((Var)node);
                    return;
                case ASTNodeType.BinOp:
                    Visit_BinOp((BinOp)node);
                    return;
                case ASTNodeType.UnOp:
                    Visit_UnOp((UnOp)node);
                    return;
                case ASTNodeType.Num:
                    return;
                case ASTNodeType.NoOp:
                    return;
                default:
                    throw new Exception("SymbolTableBuilder could not match node type");
            }
        }

        private static void Visit_Program(Program node)
        {
            mCurrentScope = new ScopedSymbolTable("global", 1, mCurrentScope);
            Visit(node.Block);
            DumpCurrentScope();
            mCurrentScope = mCurrentScope.EnclosingScope;
        }

        private static void Visit_Block(Block node)
        {
            foreach (AST.Node n in node.DeclarationNodes)
                Visit(n);

            Visit(node.CompoundStatement);
        }

        private static void Visit_ProcedureDecl(ProcedureDecl node)
        {
            string proc_name = node.ProcedureName;
            ProcedureSymbol proc_symbol = new ProcedureSymbol(proc_name);
            mCurrentScope.Insert(proc_symbol);

            //Enter parameter/local variable scope
            mCurrentScope = new ScopedSymbolTable(proc_name, mCurrentScope.ScopeLevel + 1, mCurrentScope);

            foreach (Param p in node.Parameters)
            {
                //Is this going to work?
                Symbol param_type = mCurrentScope.Lookup(p.TypeNode.Value);
                string param_name = p.VarNode.Name;
                Symbol var_symbol = new VarSymbol(param_name, param_type);
                mCurrentScope.Insert(var_symbol);
                proc_symbol.FormalParameters.Add(var_symbol);
            }

            Visit(node.BlockNode);

            DumpCurrentScope();

            mCurrentScope = mCurrentScope.EnclosingScope;

            proc_symbol.BlockNode = node.BlockNode;
        }

        private static void Visit_ProcedureCall(ProcedureCall node)
        {
            foreach (AST.Node param_node in node.ActualParameters)
                Visit(param_node);

            ProcedureSymbol proc_symbol = (ProcedureSymbol)mCurrentScope.Lookup(node.ProcedureName);
            var formal_params = proc_symbol.FormalParameters;
            var actual_params = node.ActualParameters;

            if (formal_params.Count != actual_params.Count)
                throw new Exception(string.Format("Semantic Analyzer error when calling {0}", node.ProcedureName));

            node.ProcedureSymbol = proc_symbol;
        }

        private static void Visit_VarDecl(VarDecl node)
        {
            string type_name = node.TypeNode.Value;
            Symbol type_symbol = mCurrentScope.Lookup(type_name);

            string var_name = node.VarNode.Name;
            Symbol var_symbol = new VarSymbol(var_name, type_symbol);

            //Error on redeclarations
            if (mCurrentScope.Lookup(var_name, true) != null)
                throw new Exception(string.Format("SemanticAnalyzer error: variable {0} already defined", var_name));

            mCurrentScope.Insert(var_symbol);
        }

        private static void Visit_Compound(Compound node)
        {
            foreach (Node n in node.Nodes)
                Visit(n);
        }

        private static void Visit_Assign(Assign node)
        {
            string var_name = node.Left.Name;
            Symbol var_symbol = mCurrentScope.Lookup(var_name);
            if (var_symbol == null)
                throw new Exception(String.Format("Symbol {0} not defined", var_name));

            Visit(node.Right);
        }

        private static void Visit_Var(Var node)
        {
            string var_name = node.Name;
            Symbol var_symbol = mCurrentScope.Lookup(var_name);
            if (var_symbol == null)
                throw new Exception(String.Format("Symbol {0} not defined", var_name));
        }

        private static void Visit_BinOp(BinOp node)
        {
            Visit(node.Left);
            Visit(node.Right);
        }

        private static void Visit_UnOp(UnOp node)
        {
            Visit(node.Right);
        }

        private static void DumpCurrentScope()
        {
            Console.WriteLine(mCurrentScope);
        }
    }
}
