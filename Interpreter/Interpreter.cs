﻿using System;
using System.Collections.Generic;
using Interpreter.AST;
using Interpreter.Internal;

namespace Interpreter
{

    public static class Interpreter
    {
        #region Private Members

        static private CallStack mCallStack;

        #endregion

        #region Public Fns

        public static dynamic Visit(Node node)
        {
            switch (node.Type)
            {
                case ASTNodeType.None:
                    throw new Exception("Parser error: NodeVisitor tried to visit a generic node");
                case ASTNodeType.Program:
                    return Visit_Program((Program)node);
                case ASTNodeType.Block:
                    return Visit_Block((Block)node);
                case ASTNodeType.VarDecl:
                    return Visit_VarDecl((VarDecl)node);
                case ASTNodeType.ProcedureDecl:
                    return Visit_ProcedureDecl((ProcedureDecl)node);
                case ASTNodeType.ProcedureCall:
                    return Visit_ProcedureCall((ProcedureCall)node);
                case ASTNodeType.TypeNode:
                    return Visit_Type((TypeNode)node);
                case ASTNodeType.Compound:
                    return Visit_Compound((Compound)node);
                case ASTNodeType.Assign:
                    return Visit_Assign((Assign)node);
                case ASTNodeType.Var:
                    return Visit_Var((Var)node);
                case ASTNodeType.BinOp:
                    return Visit_BinOp((BinOp)node);
                case ASTNodeType.UnOp:
                    return Visit_UnOp((UnOp)node);
                case ASTNodeType.Num:
                    return Visit_Num((Num)node);
                case ASTNodeType.NoOp:
                    return Visit_NoOp((NoOp)node);
                default:
                    throw new Exception("Parser error: NodeVisitor could not match node type");
            }
        }

        #endregion

        #region Constructor

        static Interpreter()
        {
            mCallStack = new CallStack();
        }

        #endregion

        #region Private Fns

        private static int Visit_Program(Program node)
        {
            string program_name = node.Name;

            ActivationRecord ar = new ActivationRecord(program_name, ARType.PROGRAM, 1);
            mCallStack.Push(ar);
            
            Visit(node.Block);

            mCallStack.Pop();
            return 0;
        }

        private static int Visit_Block(Block node)
        {
            foreach (AST.Node n in node.DeclarationNodes)
                Visit(n);

            return (int)Visit(node.CompoundStatement);
        }

        private static int Visit_VarDecl(VarDecl node)
        {
            return 0;
        }

        private static int Visit_ProcedureDecl(ProcedureDecl node)
        {
            return 0;
        }

        private static int Visit_ProcedureCall(ProcedureCall node)
        {
            string proc_name = node.ProcedureName;
            ProcedureSymbol proc_symbol = node.ProcedureSymbol;
            ActivationRecord ar = new ActivationRecord(proc_name, ARType.PROCEDURE, proc_symbol.ScopeLevel + 1);

            //Grab procedure and both expected and actual parameter lists
            List<Symbol> formal_params = proc_symbol.FormalParameters;
            List<AST.Node> actual_params = node.ActualParameters;

            //Load parameters into ActivationRecord
            for (int i = 0; i < actual_params.Count; i++)
                ar[formal_params[i].Name] = Visit(actual_params[i]);

            //Push ActivationRecord onto CallStack, run Procedure Block,
            //then pop the record back off
            mCallStack.Push(ar);
            Visit(proc_symbol.BlockNode);
            mCallStack.Pop();

            return 0;
        }

        private static int Visit_Type(TypeNode node)
        {
            return 0;
        }

        private static int Visit_Compound(Compound node)
        {
            foreach (Node n in node.Nodes)
                Visit(n);

            return 0;
        }

        private static int Visit_Assign(Assign node)
        {
            string var_name = node.Left.Name;
            dynamic var_value = Visit(node.Right);

            ActivationRecord ar = mCallStack.Peek();
            ar[var_name] = var_value;

            return 0;
        }

        private static dynamic Visit_Var(Var node)
        {
            string var_name = node.Name;

            ActivationRecord ar = mCallStack.Peek();
            dynamic var_value = ar[var_name];

            //Be aware of this, it could cause problems with null values
            if (var_value == null)
                throw new Exception("Parser error: variable not defined");
            else
                return var_value;
        }

        private static float Visit_BinOp(BinOp node)
        {
            switch (node.Op.Type)
            {
                case TokenType.PLUS:
                    return Visit(node.Left) + Visit(node.Right);
                case TokenType.MINUS:
                    return Visit(node.Left) - Visit(node.Right);
                case TokenType.MULT:
                    return Visit(node.Left) * Visit(node.Right);
                case TokenType.INTEGER_DIV:
                    return (int)Visit(node.Left) / (int)Visit(node.Right);
                case TokenType.FLOAT_DIV:
                    return (float)Visit(node.Left) / (float)Visit(node.Right);
                default:
                    throw new Exception("BinOp node visit failure");
            }
        }

        private static float Visit_UnOp(UnOp node)
        {
            switch (node.Op.Type)
            {
                case TokenType.PLUS:
                    return +Visit(node.Right);
                case TokenType.MINUS:
                    return -Visit(node.Right);
                default:
                    throw new Exception("UnOp node visit failure");
            }
        }

        private static float Visit_Num(Num node)
        {
            return node.Value;
        }

        private static int Visit_NoOp(NoOp node)
        {
            return 0;
        }

        #endregion
    }
}
