﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class Lexer
    {
        #region Private Members

        private string mInput;
        private int mPos;
        private char mCurrentChar;

        //Position markers
        private int mLineNo;
        private int mColumnNo;

        //Pascal character reservation
        Dictionary<string, TokenType> mReservedKeywords = new Dictionary<string, TokenType>
        {
            { "PROGRAM", TokenType.PROGRAM },
            { "VAR", TokenType.VAR },
            { "DIV", TokenType.INTEGER_DIV },
            { "INTEGER", TokenType.INTEGER },
            { "REAL", TokenType.REAL },
            { "BEGIN", TokenType.BEGIN },
            { "END", TokenType.END },
            { "PROCEDURE", TokenType.PROCEDURE }
        };

        #endregion

        #region

        public char CurrentChar { get { return mCurrentChar; } }

        #endregion

        #region Public Fns

        public Token GetNextToken()
        {
            while (mCurrentChar != '~')
            {
                //White space skipping
                if (Char.IsWhiteSpace(mCurrentChar))
                {
                    SkipWhiteSpace();
                    continue;
                }

                //Comments
                if (mCurrentChar == '{')
                {
                    Advance();
                    SkipComment();
                    continue;
                }

                if ((mCurrentChar == ':') && (Peek() == '='))
                {
                    Advance(); Advance();
                    return new Token(TokenType.ASSIGN, null, mLineNo, mColumnNo);
                }

                //Numeric building blocks
                if (Char.IsDigit(mCurrentChar))
                    return Number();

                if (mCurrentChar == ':')
                {
                    Advance();
                    return new Token(TokenType.COLON, null, mLineNo, mColumnNo);
                }

                if (mCurrentChar == ',')
                {
                    Advance();
                    return new Token(TokenType.COMMA, null, mLineNo, mColumnNo);
                }

                //Standard arithmetic operations
                if (mCurrentChar == '+')
                {
                    Advance();
                    return new Token(TokenType.PLUS, null, mLineNo, mColumnNo);
                }

                if (mCurrentChar == '-')
                {
                    Advance();
                    return new Token(TokenType.MINUS, null, mLineNo, mColumnNo);
                }

                if (mCurrentChar == '*')
                {
                    Advance();
                    return new Token(TokenType.MULT, null, mLineNo, mColumnNo);
                }

                if (mCurrentChar == '/')
                {
                    Advance();
                    return new Token(TokenType.FLOAT_DIV, null, mLineNo, mColumnNo);
                }

                //Parenthesis
                if (mCurrentChar == '(')
                {
                    Advance();
                    return new Token(TokenType.LPAREN, null, mLineNo, mColumnNo);
                }

                if (mCurrentChar == ')')
                {
                    Advance();
                    return new Token(TokenType.RPAREN, null, mLineNo, mColumnNo);
                }

                if (Char.IsLetter(mCurrentChar))
                    return Id();

                if (mCurrentChar == ';')
                {
                    Advance();
                    return new Token(TokenType.SEMI, null, mLineNo, mColumnNo);
                }

                if (mCurrentChar == '.')
                {
                    Advance();
                    return new Token(TokenType.DOT, null, mLineNo, mColumnNo);
                }

                throw new Exception(string.Format("Lexer error: '{0}' @l{1}c{2} symbol matched no tokens", mCurrentChar, mLineNo, mColumnNo));
            }

            return new Token(TokenType.EOF, null, mLineNo, mColumnNo);
        }

        #endregion

        #region Constructor

        public Lexer(string input)
        {
            mInput = input;
            mPos = 0;
            mCurrentChar = mInput[mPos];

            mLineNo = 1;
            mColumnNo = 1;
        }

        #endregion

        #region Private Fns

        private void Advance()
        {
            if ((mCurrentChar == '\n') || (mCurrentChar == '\r'))
            {
                mLineNo++;
                mColumnNo = 0;
            }
            
            mPos++;
            if (mPos > mInput.Length - 1)
                mCurrentChar = '~';
            else
            {
                mCurrentChar = mInput[mPos];
                mColumnNo++;
            }
        }

        private char Peek()
        {
            int peek_pos = mPos + 1;
            if (peek_pos > mInput.Length + 1)
                return '~';
            else
                return mInput[peek_pos];
        }

        private void SkipWhiteSpace()
        {
            while ((mCurrentChar != '~') && (Char.IsWhiteSpace(mCurrentChar)))
                Advance();
        }

        private void SkipComment()
        {
            while (mCurrentChar != '}')
                Advance(); //Advance through comment
            Advance(); //Closing curly brace
        }

        private Token Number()
        {
            string result = "";
            while ((mCurrentChar != '~') && (Char.IsDigit(mCurrentChar)))
            {
                result += mCurrentChar;
                Advance();
            }

            if (mCurrentChar == '.')
            {
                result += mCurrentChar;
                Advance();

                while ((mCurrentChar != '~') && (Char.IsDigit(mCurrentChar)))
                {
                    result += mCurrentChar;
                    Advance();
                }

                return new Token(TokenType.REAL_CONST, float.Parse(result), mLineNo, mColumnNo);
            }
            else
                return new Token(TokenType.INTEGER_CONST, int.Parse(result), mLineNo, mColumnNo);
        }

        private Token Id()
        {
            string result = "";

            while ((mCurrentChar != '~') && (char.IsLetterOrDigit(mCurrentChar)))
            {
                result += mCurrentChar;
                Advance();
            }

            //Case insensitive keywords, dict to convert to type
            string upper_result = result.ToUpper();
            if (mReservedKeywords.Keys.ToList().Contains(upper_result))
                return new Token(mReservedKeywords[upper_result], upper_result, mLineNo, mColumnNo);
            else
                return new Token(TokenType.ID, result, mLineNo, mColumnNo);
        }

        #endregion
    }
}
