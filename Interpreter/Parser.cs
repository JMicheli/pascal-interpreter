﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    public class Parser
    {
        #region Private Members

        private Lexer mLexer;
        private Token mCurrentToken;

        #endregion

        #region Public Fns

        /// <summary>
        /// This will be the entry point for the Interpreter
        /// </summary>
        /// <returns>The result of the interpretation</returns>
        public AST.Node Parse()
        {
            var node = Program();
            if (mCurrentToken.Type != TokenType.EOF)
                throw new Exception("Parser error: no EOF after Program");

            return node;
        }

        #endregion

        #region Constructor

        public Parser(string input)
        {
            mLexer = new Lexer(input);
            mCurrentToken = mLexer.GetNextToken();
        }

        #endregion

        #region Private Fns

        private void Eat(TokenType type)
        {
            if (mCurrentToken.Type == type)
                mCurrentToken = mLexer.GetNextToken();
            else
                throw new Exception(string.Format("Parser error: {0} unexpected token", mCurrentToken));
        }

        private AST.Node Program()
        {
            Eat(TokenType.PROGRAM);
            AST.Var var_node = Variable();
            string prog_name = var_node.Name;
            Eat(TokenType.SEMI);
            AST.Block block_node = Block();
            AST.Program program_node = new AST.Program(prog_name, block_node);
            Eat(TokenType.DOT);

            return program_node;
        }

        private AST.Block Block()
        {
            List<AST.Node> declarations = Declarations();
            AST.Compound compound = CompoundStatement();
            return new AST.Block(declarations, compound);
        }

        private List<AST.Node> Declarations()
        {
            List<AST.Node> declarations = new List<AST.Node>();

            while (true)
            {
                if (mCurrentToken.Type == TokenType.VAR)
                {
                    Eat(TokenType.VAR);
                    while (mCurrentToken.Type == TokenType.ID)
                    {
                        List<AST.VarDecl> var_decls = VariableDeclaration();
                        foreach (AST.Node n in var_decls)
                            declarations.Add(n);

                        Eat(TokenType.SEMI);
                    }
                }
                else if (mCurrentToken.Type == TokenType.PROCEDURE)
                {
                    AST.ProcedureDecl proc_decl = ProcedureDeclaration();
                    declarations.Add(proc_decl);
                }
                else
                    break;
            }

            return declarations;
        }

        private AST.ProcedureDecl ProcedureDeclaration()
        {
            Eat(TokenType.PROCEDURE);
            string proc_name = mCurrentToken.Value;
            Eat(TokenType.ID);
            List<AST.Param> parameters = new List<AST.Param>();

            if (mCurrentToken.Type == TokenType.LPAREN)
            {
                Eat(TokenType.LPAREN);
                parameters = FormalParameterList();
                Eat(TokenType.RPAREN);
            }

            Eat(TokenType.SEMI);
            AST.Block block_node = Block();
            Eat(TokenType.SEMI);
            return new AST.ProcedureDecl(proc_name, parameters, block_node);
        }

        private AST.ProcedureCall ProcedureCallStatement()
        {
            Token token = mCurrentToken;

            string proc_name = mCurrentToken.Value;
            Eat(TokenType.ID);
            Eat(TokenType.LPAREN);

            List<AST.Node> actual_parameters = new List<AST.Node>();

            if (mCurrentToken.Type != TokenType.RPAREN)
            {
                var node = Expression();
                actual_parameters.Add(node);
            }

            while (mCurrentToken.Type == TokenType.COMMA)
            {
                Eat(TokenType.COMMA);
                var node = Expression();
                actual_parameters.Add(node);
            }

            Eat(TokenType.RPAREN);

            return new AST.ProcedureCall(proc_name, actual_parameters, token);
        }

        private List<AST.Param> FormalParameterList()
        {
            List<AST.Param> param_nodes = new List<AST.Param>();
            if (mCurrentToken.Type != TokenType.ID)
                return param_nodes;

            param_nodes = FormalParameters();

            while (mCurrentToken.Type == TokenType.SEMI)
            {
                Eat(TokenType.SEMI);
                List<AST.Param> nodes = FormalParameters();
                foreach (AST.Param node in nodes)
                    param_nodes.Add(node);
            }

            return param_nodes;
        }

        private List<AST.Param> FormalParameters()
        {
            List<AST.Param> param_nodes = new List<AST.Param>();

            List<Token> param_tokens = new List<Token>();
            param_tokens.Add(mCurrentToken);

            Eat(TokenType.ID);
            while (mCurrentToken.Type == TokenType.COMMA)
            {
                Eat(TokenType.COMMA);
                param_tokens.Add(mCurrentToken);
                Eat(TokenType.ID);
            }

            Eat(TokenType.COLON);
            AST.TypeNode type_node = TypeSpec();

            foreach (Token param_token in param_tokens)
            {
                AST.Param param_node = new AST.Param(new AST.Var(param_token), type_node);
                param_nodes.Add(param_node);
            }

            return param_nodes;
        }

        private List<AST.VarDecl> VariableDeclaration()
        {
            List<AST.Var> var_nodes = new List<AST.Var>();
            var_nodes.Add(new AST.Var(mCurrentToken));
            Eat(TokenType.ID);

            while (mCurrentToken.Type == TokenType.COMMA)
            {
                Eat(TokenType.COMMA);
                var_nodes.Add(new AST.Var(mCurrentToken));
                Eat(TokenType.ID);
            }

            Eat(TokenType.COLON);

            AST.TypeNode type_node = TypeSpec();
            List<AST.VarDecl> var_declarations = new List<AST.VarDecl>();
            foreach (AST.Var var in var_nodes)
                var_declarations.Add(new AST.VarDecl(var, type_node));

            return var_declarations;
        }

        private AST.TypeNode TypeSpec()
        {
            Token token = mCurrentToken;

            if (mCurrentToken.Type == TokenType.INTEGER)
                Eat(TokenType.INTEGER);
            else
                Eat(TokenType.REAL);

            return new AST.TypeNode(token);
        }

        private AST.Compound CompoundStatement()
        {
            Eat(TokenType.BEGIN);
            List<AST.Node> nodes = StatementList();
            Eat(TokenType.END);

            return new AST.Compound(nodes);
        }

        private List<AST.Node> StatementList()
        {
            AST.Node node = Statement();

            var results = new List<AST.Node>();
            results.Add(node);

            while (mCurrentToken.Type == TokenType.SEMI)
            {
                Eat(TokenType.SEMI);
                results.Add(Statement());
            }

            if (mCurrentToken.Type == TokenType.ID)
                throw new Exception(string.Format("Parser error: {0} ID after statementlist", mCurrentToken));

            return results;
        }

        private AST.Node Statement()
        {
            AST.Node node;
            if (mCurrentToken.Type == TokenType.BEGIN)
                node = CompoundStatement();
            else if ((mCurrentToken.Type == TokenType.ID) && (mLexer.CurrentChar == '('))
                node = ProcedureCallStatement();
            else if (mCurrentToken.Type == TokenType.ID)
                node = AssignmentStatement();
            else
                node = Empty();

            return node;
        }

        private AST.Node AssignmentStatement()
        {
            AST.Var left = Variable();
            Token token = mCurrentToken;
            Eat(TokenType.ASSIGN);
            AST.Node right = Expression();

            return new AST.Assign(left, token, right);
        }

        private AST.Var Variable()
        {
            AST.Var node = new AST.Var(mCurrentToken);
            Eat(TokenType.ID);
            return node;
        }

        private AST.Node Expression()
        {
            var node = Term();

            while ((mCurrentToken.Type == TokenType.PLUS) || (mCurrentToken.Type == TokenType.MINUS))
            {
                Token token = mCurrentToken;
                if (token.Type == TokenType.PLUS)
                    Eat(TokenType.PLUS);
                else if (token.Type == TokenType.MINUS)
                    Eat(TokenType.MINUS);

                node = new AST.BinOp(node, token, Term());
            }

            return node;
        }

        private AST.Node Term()
        {
            var node = Factor();

            while ((mCurrentToken.Type == TokenType.MULT) || (mCurrentToken.Type == TokenType.INTEGER_DIV) || (mCurrentToken.Type == TokenType.FLOAT_DIV))
            {
                Token token = mCurrentToken;
                if (token.Type == TokenType.MULT)
                    Eat(TokenType.MULT);
                else if (token.Type == TokenType.INTEGER_DIV)
                    Eat(TokenType.INTEGER_DIV);
                else if (token.Type == TokenType.FLOAT_DIV)
                    Eat(TokenType.FLOAT_DIV);

                node = new AST.BinOp(node, token, Factor());
            }

            return node;
        }

        private AST.Node Factor()
        {
            Token token = mCurrentToken;

            if (mCurrentToken.Type == TokenType.PLUS)
            {
                Eat(TokenType.PLUS);
                return new AST.UnOp(token, Factor());
            }
            else if (mCurrentToken.Type == TokenType.MINUS)
            {
                Eat(TokenType.MINUS);
                return new AST.UnOp(token, Factor());
            }
            else if (mCurrentToken.Type == TokenType.INTEGER_CONST)
            {
                Eat(TokenType.INTEGER_CONST);
                return new AST.Num(token);
            }
            else if (mCurrentToken.Type == TokenType.REAL_CONST)
            {
                Eat(TokenType.REAL_CONST);
                return new AST.Num(token);
            }
            else if (mCurrentToken.Type == TokenType.LPAREN)
            {
                Eat(TokenType.LPAREN);
                var node = Expression();
                Eat(TokenType.RPAREN);
                return node;
            }
            else
                return Variable();
        }

        private AST.Node Empty()
        {
            return new AST.NoOp();
        }

        #endregion
    }
}
