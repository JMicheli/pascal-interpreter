﻿namespace Interpreter
{
    class Token
    {
        #region Public Properties

        public TokenType Type { get; private set; }
        public dynamic Value { get; private set; }

        public int LineNo { get; private set; }
        public int ColumnNo { get; private set; }

        #endregion

        #region Public Overloads

        public override string ToString()
        {
            return string.Format("Token({0}, {1}) @l{2}c{3}", Type.ToString("F"), Value, LineNo, ColumnNo);
        }

        #endregion

        #region Constructor

        public Token(TokenType type, dynamic value, int line, int col)
        {
            Type = type; Value = value;
            LineNo = line; ColumnNo = col;
        }

        #endregion
    }
}
