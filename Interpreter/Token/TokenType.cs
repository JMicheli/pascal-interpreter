﻿namespace Interpreter
{
    public enum TokenType
    {
        INTEGER,
        REAL,
        INTEGER_CONST,
        REAL_CONST,

        PLUS,
        MINUS,
        MULT,
        INTEGER_DIV,
        FLOAT_DIV,
        LPAREN,
        RPAREN,

        ID,
        ASSIGN,
        BEGIN,
        END,
        PROCEDURE,
        SEMI,
        DOT,
        PROGRAM,
        VAR,
        COLON,
        COMMA,

        EOF
    }
}
