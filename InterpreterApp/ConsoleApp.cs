﻿using System;
using System.IO;

using Interpreter;
using Interpreter.Internal;

namespace InterpreterApp
{
    class ConsoleAPP
    {
        static void Main(string[] args)
        {
            InputLoop();
        }

        private static void InputLoop()
        {
            while (true)
            {
                string input = Console.ReadLine();

                //Commands
                switch(input.ToLower())
                {
                    case "": //Skip blank lines
                        continue;
                    case "exit": //Exit on "exit"
                        return;
                    default:
                        RunInput(input);
                        continue;
                }
            }
        }

        private static void RunInput(string input)
        {
            //Open code file, exit if it can't be opened
            string code = "";
            try
            {
                code = File.ReadAllText(input);
            }
            catch
            {
                Console.WriteLine("File could not be opened");
                return;
            }

            //Prepare to run
            SemanticAnalyzer.ClearGlobal();
            Parser parser = new Parser(code);
            string output = "";

            //Attempt to run
            try
            {
                var tree = parser.Parse();
                SemanticAnalyzer.Visit(tree);
                var result = Interpreter.Interpreter.Visit(tree);
                output = result.ToString();
            }
            catch (Exception e)
            {
                output = e.Message;
            }

            //Output results
            Console.WriteLine(output);
        }
    }
}
